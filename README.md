# README #

This project was created during a live coding session for the lecture "Software Engineering für betriebliche Anwendungen – Masterkurs: Web Application Engineering" (IN2087)	at TUM. It is a brief introduction in the Play Framework 2.x (Java) and AngularJS.
Students at TUM who are enrolled in this course can find additional resources and a video-recording of these live-coding sessions on moodle. 

## What examples can be found here? ##

This project starts from the hello-play-java activator template ([http://www.typesafe.com/activator/template/hello-play-java](http://www.typesafe.com/activator/template/hello-play-java)). On the master branch (first session) you can find a extension of the template with added functionality such as models, front-end views and routes/controller methods, while the angular_app branch (secound session) is holding a RESTful API with AngularJS in the frontend. The following libraries were used:

### backend: ###
* Play Framework (Java) [https://www.playframework.com/](https://www.playframework.com/)
* JPA [https://www.playframework.com/documentation/2.3.x/JavaJPA](https://www.playframework.com/documentation/2.3.x/JavaJPA)
* Webjars https://www.playframework.com/documentation/2.3.x/Assets

### frontend: ###
* bootstrap [http://getbootstrap.com/](http://getbootstrap.com/)
* AngularJS (Angular UIRouter, JS-Data) [https://angularjs.org/](https://angularjs.org/), [https://github.com/angular-ui/ui-router](https://github.com/angular-ui/ui-router), [http://www.js-data.io/v1.8.0/docs/js-data-angular](http://www.js-data.io/v1.8.0/docs/js-data-angular)

## IDE Setup ##

### Intellij IDEA ###

* Make sure, the Scala plugin is installed ([https://plugins.jetbrains.com/plugin/?id=1347](https://plugins.jetbrains.com/plugin/?id=1347))
* Import Project -> From existing model -> SBT

**Important: ** If the import is not working, you have probably an old Version of the Scala Plugin (1.4.15). Try to update the Plugin to version 1.5

## Run the project ##

### Intellij ###
* right-click on any project file -> Run as Play 2 App
* Open [localhost:9000](localhost:9000) when compilation is finished

### Activator (Command Line) ###

* Navigate to your project root
* Execute `activator run`
* Open [localhost:9000](localhost:9000) when compilation is finished